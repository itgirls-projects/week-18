package ru.itgirls.libraryproject.service;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itgirls.libraryproject.createdto.AuthorCreateDTO;
import ru.itgirls.libraryproject.dto.AuthorDTO;
import ru.itgirls.libraryproject.dto.BookDTO;
import ru.itgirls.libraryproject.dtooutput.AuthorDTOOutput;
import ru.itgirls.libraryproject.model.Author;
import ru.itgirls.libraryproject.model.Book;
import ru.itgirls.libraryproject.repository.AuthorRepository;
import ru.itgirls.libraryproject.updatedto.AuthorUpdateDTO;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    final AuthorRepository authorRepository;

    @Override
    public AuthorDTO findAuthorByID(Long id) {
        Author author = authorRepository.findById(id).orElseThrow();
        return convertToDTO(author);
    }

    @Override
    public List<AuthorDTOOutput> getAllAuthors() {
        List<Author> authors = authorRepository.findAll();
        return authors.stream().map(this::convertToDTOOutput).toList();
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorByName1(String name) {
        List<Author> authorList = authorRepository.findAuthorByName(name);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorBySurname1(String surname) {
        List<Author> authorList = authorRepository.findAuthorBySurname(surname);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorByName2(String name) {
        List<Author> authorList = authorRepository.findAuthorByNameSQL(name);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorBySurname2(String surname) {
        List<Author> authorList = authorRepository.findAuthorBySurnameSQL(surname);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorByName3(String name) {
        Specification<Author> specification = Specification.where(new Specification<Author>() {
            @Override
            public Predicate toPredicate(Root<Author> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("name"), name);
            }
        });
        List<Author> authorList = authorRepository.findAll(specification);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @SneakyThrows
    @Override
    public List<AuthorDTO> getAuthorBySurname3(String surname) {
        Specification<Author> specification = Specification.where(new Specification<Author>() {
            @Override
            public Predicate toPredicate(Root<Author> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("surname"), surname);
            }
        });
        List<Author> authorList = authorRepository.findAll(specification);
        if (authorList.isEmpty()) {
            throw new Exception();
        }
        return convertToDTOList(authorList);
    }

    @Override
    public AuthorDTO createAuthor(AuthorCreateDTO authorCreateDTO) {
        Author author = authorRepository.save(convertToEntity(authorCreateDTO));
        return convertToDTO(author);
    }

    @Override
    public AuthorDTO updateAuthor(AuthorUpdateDTO authorUpdateDTO) {
        Author author = authorRepository.findById(authorUpdateDTO.getId()).orElseThrow();
        author.setName(authorUpdateDTO.getName());
        author.setSurname(authorUpdateDTO.getSurname());
        Author updateAuthor = authorRepository.save(author);
        return convertToDTO(updateAuthor);
    }

    @Override
    public void deleteAuthor(Long id) {
        authorRepository.deleteById(id);
    }

    private AuthorDTO convertToDTO(Author author) {
        List<BookDTO> bookDTOList = null;
        if (author.getBooks() != null) {
            bookDTOList = author.getBooks()
                    .stream()
                    .map(book -> BookDTO.builder()
                            .genre(book.getGenre().getName())
                            .name(book.getName())
                            .build()
                    ).toList();
        }
        return AuthorDTO.builder()
                .books(bookDTOList)
                .name(author.getName())
                .surname(author.getSurname())
                .build();
    }

    private List<AuthorDTO> convertToDTOList(List<Author> authorList) {
        List<AuthorDTO> authorDTOList = new ArrayList<>();
        for (Author author : authorList) {
            authorDTOList.add(convertToDTO(author));
        }
        return authorDTOList;
    }

    private AuthorDTOOutput convertToDTOOutput(Author author) {
        List<String> bookList = null;
        if (author.getBooks() != null) {
            bookList = author.getBooks()
                    .stream()
                    .map(Book::getName)
                    .toList();
        }
        return AuthorDTOOutput.builder()
                .id(author.getId())
                .name(author.getName())
                .surname(author.getSurname())
                .books(bookList)
                .build();
    }

    public Author convertToEntity(AuthorCreateDTO authorCreateDTO) {
        return Author.builder()
                .name(authorCreateDTO.getName())
                .surname(authorCreateDTO.getSurname())
                .build();
    }
}
