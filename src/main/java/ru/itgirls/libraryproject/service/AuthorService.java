package ru.itgirls.libraryproject.service;

import ru.itgirls.libraryproject.createdto.AuthorCreateDTO;
import ru.itgirls.libraryproject.dto.AuthorDTO;
import ru.itgirls.libraryproject.dtooutput.AuthorDTOOutput;
import ru.itgirls.libraryproject.updatedto.AuthorUpdateDTO;

import java.util.List;

public interface AuthorService {
    AuthorDTO findAuthorByID(Long id);
    List<AuthorDTOOutput> getAllAuthors();
    List<AuthorDTO> getAuthorByName1(String name);
    List<AuthorDTO> getAuthorByName2(String name);
    List<AuthorDTO> getAuthorByName3(String name);
    List<AuthorDTO> getAuthorBySurname1(String surname);
    List<AuthorDTO> getAuthorBySurname2(String surname);
    List<AuthorDTO> getAuthorBySurname3(String surname);
    AuthorDTO createAuthor(AuthorCreateDTO authorCreateDTO);
    AuthorDTO updateAuthor(AuthorUpdateDTO authorUpdateDTO);
    void deleteAuthor(Long id);
}
