package ru.itgirls.libraryproject.service;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itgirls.libraryproject.createdto.BookCreateDTO;
import ru.itgirls.libraryproject.dto.AuthorDTO;
import ru.itgirls.libraryproject.dto.BookDTO;
import ru.itgirls.libraryproject.dtooutput.BookDTOOutput;
import ru.itgirls.libraryproject.model.Author;
import ru.itgirls.libraryproject.model.Book;
import ru.itgirls.libraryproject.repository.BookRepository;
import ru.itgirls.libraryproject.updatedto.BookUpdateDTO;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    final BookRepository bookRepository;

    @Autowired
    final AuthorServiceImpl authorServiceImpl;
    @Autowired
    final GenreServiceImpl genreServiceImpl;

    @Override
    public BookDTO findBookByID(Long id) {
        Book book = bookRepository.findById(id).orElseThrow();
        return convertToDTO(book);
    }

    @Override
    public List<BookDTOOutput> getAllBooks() {
        List<Book> books = bookRepository.findAll();
        return books
                .stream()
                .map(this::convertToDTOOutput)
                .collect(Collectors.toList());
    }

    @Override
    public BookDTO getBookByName1(String name) {
        Book book = bookRepository.findBookByName(name).orElseThrow();
        return convertToDTO(book);
    }

    @Override
    public BookDTO getBookByName2(String name) {
        Book book = bookRepository.findBookByNameSQL(name).orElseThrow();
        return convertToDTO(book);
    }

    @Override
    public BookDTO getBookByName3(String name) {
        Specification<Book> specification = Specification.where(new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("name"), name);
            }
        });

        Book book = bookRepository.findOne(specification).orElseThrow();
        return convertToDTO(book);
    }

    @Override
    public BookDTO createBook(BookCreateDTO bookCreateDTO) {
        Book book = bookRepository.save(convertToEntity(bookCreateDTO));
        for (Author i : book.getAuthors()) {
            i.getBooks().add(book);
        }
        book.getGenre().getBooks().add(book);
        return convertToDTO(book);
    }

    public BookDTO updateBook(BookUpdateDTO bookUpdateDTO) {
        Book book = bookRepository.findById(bookUpdateDTO.getId()).orElseThrow();
        book.setName(bookUpdateDTO.getName());
        book.setGenre(genreServiceImpl.genreRepository.findById(bookUpdateDTO.getGenreID()).orElseThrow());
        book.setAuthors(bookUpdateDTO.getAuthorID()
                        .stream()
                        .map(authorID -> authorServiceImpl.authorRepository.findById(authorID).orElseThrow())
                        .collect(Collectors.toSet()));
        Book updateBook = bookRepository.save(book);
        return convertToDTO(updateBook);
    }

    @Override
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public BookDTO convertToDTO(Book book) {
        return BookDTO.builder()
                //.id(book.getId())
                .name(book.getName())
                .genre(book.getGenre().getName())
                .authors(book.getAuthors()
                        .stream()
                        .map(author -> AuthorDTO.builder()
                                .name(author.getName())
                                .surname(author.getSurname())
                                .build())
                        .toList())
                .build();
    }

    public BookDTOOutput convertToDTOOutput(Book book) {
        return BookDTOOutput.builder()
                .id(book.getId())
                .name(book.getName())
                .genre(book.getGenre().getName())
                .authors(book.getAuthors()
                        .stream()
                        .map(author -> String.join(" ", author.getName(), author.getSurname()))
                        .toList())
                .build();
    }

    public Book convertToEntity(BookCreateDTO bookcreateDTO) {
        return Book.builder()
                .name(bookcreateDTO.getName())
                .genre(genreServiceImpl.genreRepository.findById(bookcreateDTO.getGenreID()).orElseThrow())
                .authors(bookcreateDTO.getAuthorID()
                        .stream()
                        .map(authorID -> authorServiceImpl.authorRepository.findById(authorID).orElseThrow())
                        .collect(Collectors.toSet()))
                .build();
    }
}
