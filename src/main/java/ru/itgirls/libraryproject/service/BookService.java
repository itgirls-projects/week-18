package ru.itgirls.libraryproject.service;

import ru.itgirls.libraryproject.createdto.BookCreateDTO;
import ru.itgirls.libraryproject.dto.BookDTO;
import ru.itgirls.libraryproject.dtooutput.BookDTOOutput;
import ru.itgirls.libraryproject.updatedto.BookUpdateDTO;

import java.util.List;

public interface BookService {
    BookDTO findBookByID(Long id);
    List<BookDTOOutput> getAllBooks();
    BookDTO getBookByName1(String name);
    BookDTO getBookByName2(String name);
    BookDTO getBookByName3(String name);
    BookDTO createBook(BookCreateDTO bookCreateDTO);
    BookDTO updateBook(BookUpdateDTO bookUpdateDTO);
    void deleteBook(Long id);
}
