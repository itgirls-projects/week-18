package ru.itgirls.libraryproject.service;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itgirls.libraryproject.dto.AuthorDTO;
import ru.itgirls.libraryproject.dto.BookDTO;
import ru.itgirls.libraryproject.dto.GenreDTO;
import ru.itgirls.libraryproject.model.Book;
import ru.itgirls.libraryproject.model.Genre;
import ru.itgirls.libraryproject.repository.GenreRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Primary
public class GenreServiceImpl implements GenreService {

    public final GenreRepository genreRepository;

    @Override
    public GenreDTO findGenreByID(Long id) {
        Genre genre = genreRepository.findById(id).orElseThrow();
        return convertToDTO(genre);
    }

    @Override
    public GenreDTO getGenreByName1(String name) {
        Genre genre = genreRepository.findGenreByName(name).orElseThrow();
        return convertToDTO(genre);
    }

    @Override
    public GenreDTO getGenreByName2(String name) {
        Genre genre = genreRepository.findGenreByNameSQL(name).orElseThrow();
        return convertToDTO(genre);
    }

    @Override
    public GenreDTO getGenreByName3(String name) {
        Specification<Genre> specification = Specification.where(new Specification<Genre>() {
            @Override
            public Predicate toPredicate(Root<Genre> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("name"), name);
            }
        });
        Genre genre = genreRepository.findOne(specification).orElseThrow();
        return convertToDTO(genre);
    }

    public GenreDTO convertToDTO(Genre genre) {
        List<BookDTO> bookDTOList = genre.getBooks()
                .stream()
                .map(book -> BookDTO.builder()
                        .name(book.getName())
                        // .id(book.getId())
                        .authors(book.getAuthors()
                                .stream()
                                .map(author -> AuthorDTO.builder()
                                        .name(author.getName())
                                        .surname(author.getSurname())
                                        .build())
                                .toList())
                        .build())
                .toList();

        return GenreDTO.builder()
                .books(bookDTOList)
                //.id(genre.getId())
                .name(genre.getName())
                .build();
    }
}
