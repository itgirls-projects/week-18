package ru.itgirls.libraryproject.service;

import ru.itgirls.libraryproject.dto.GenreDTO;

public interface GenreService {
    GenreDTO findGenreByID(Long id);
    GenreDTO getGenreByName1(String name);
    GenreDTO getGenreByName2(String name);
    GenreDTO getGenreByName3(String name);
    }
