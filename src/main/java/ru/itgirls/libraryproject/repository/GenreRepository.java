package ru.itgirls.libraryproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import ru.itgirls.libraryproject.model.Genre;

import java.util.Optional;

public interface GenreRepository extends JpaRepository<Genre, Long>, JpaSpecificationExecutor<Genre> {
    Optional<Genre> findGenreByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM GENRE WHERE name = ?")
    Optional<Genre> findGenreByNameSQL(String name);
}
