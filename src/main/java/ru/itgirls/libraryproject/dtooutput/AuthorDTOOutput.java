package ru.itgirls.libraryproject.dtooutput;

import lombok.*;

import java.util.List;

@Data
@Builder
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthorDTOOutput {
    Long id;
    String name;
    String surname;

    List<String> books;
}
