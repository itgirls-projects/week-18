package ru.itgirls.libraryproject.dtooutput;

import lombok.*;
import ru.itgirls.libraryproject.dto.AuthorDTO;

import java.util.List;

@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BookDTOOutput {
    private Long id;
    private String name;
    private String genre;

    private List<String> authors;
}
