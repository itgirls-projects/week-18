package ru.itgirls.libraryproject.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cascade;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(
            fetch = FetchType.LAZY
            /*cascade = CascadeType.ALL*/)
    @JoinColumn(name = "genre_id")
    private List<Book> books;
}
