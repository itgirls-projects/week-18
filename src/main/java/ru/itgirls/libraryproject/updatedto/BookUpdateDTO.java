package ru.itgirls.libraryproject.updatedto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itgirls.libraryproject.dto.AuthorDTO;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookUpdateDTO {
    private Long id;
    private String name;
    private Long genreID;

    private List<Long> authorID;
}
