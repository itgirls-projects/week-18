package ru.itgirls.libraryproject.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.itgirls.libraryproject.createdto.BookCreateDTO;
import ru.itgirls.libraryproject.dto.BookDTO;
import ru.itgirls.libraryproject.service.BookService;
import ru.itgirls.libraryproject.updatedto.BookUpdateDTO;

@RestController
@RequiredArgsConstructor
public class BookRestController {
    private final BookService bookService;

    @GetMapping("/book/id/{id}")
    BookDTO getBookByID(@PathVariable("id") Long id) {
        return bookService.findBookByID(id);
    }

    @GetMapping("/book/name/{name}")
    BookDTO getBookByName1(@PathVariable("name") String name) {
        return bookService.getBookByName1(name);
    }

    @GetMapping("/book/name/sql/{name}")
    BookDTO getBookByName2(@PathVariable("name") String name) {
        return bookService.getBookByName2(name);
    }

    @GetMapping("/book/name/jpa/{name}")
    BookDTO getBookByName3(@PathVariable("name") String name) {
        return bookService.getBookByName3(name);
    }

    @PostMapping("/book/create")
    BookDTO createBook(@RequestBody BookCreateDTO bookCreateDTO) {
        return bookService.createBook(bookCreateDTO);
    }

    @PutMapping("book/update")
    BookDTO updateBook(@RequestBody BookUpdateDTO bookUpdateDTO) {
        return bookService.updateBook(bookUpdateDTO);
    }

    @DeleteMapping("book/delete/id/{id}")
    void deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
    }
}
