package ru.itgirls.libraryproject.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.itgirls.libraryproject.createdto.AuthorCreateDTO;
import ru.itgirls.libraryproject.dto.AuthorDTO;
import ru.itgirls.libraryproject.service.AuthorService;
import ru.itgirls.libraryproject.updatedto.AuthorUpdateDTO;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AuthorRestController {
    private final AuthorService authorService;

    @GetMapping("/author/id/{id}")
    AuthorDTO findAuthorByID(@PathVariable("id") Long id) {
        return authorService.findAuthorByID(id);
    }

    @GetMapping("/author/name/{name}")
    List<AuthorDTO> getAuthorByName1(@PathVariable("name") String name) {
        return authorService.getAuthorByName1(name);
    }

    @GetMapping("/author/name/sql/{name}")
    List<AuthorDTO> getAuthorByName2(@PathVariable("name") String name) {
        return authorService.getAuthorByName2(name);
    }

    @GetMapping("/author/name/jpa/{name}")
    List<AuthorDTO> getAuthorByName3(@PathVariable("name") String name) {
        return authorService.getAuthorByName3(name);
    }

    @GetMapping("/author/surname/{surname}")
    List<AuthorDTO> getAuthorBySurname1(@PathVariable("surname") String surname) {
        return authorService.getAuthorBySurname1(surname);
    }

    @GetMapping("/author/surname/sql/{surname}")
    List<AuthorDTO> getAuthorBySurname2(@PathVariable("surname") String surname) {
        return authorService.getAuthorBySurname2(surname);
    }

    @GetMapping("/author/surname/jpa/{surname}")
    List<AuthorDTO> getAuthorBySurname3(@PathVariable("surname") String surname) {
        return authorService.getAuthorBySurname3(surname);
    }

    @PostMapping("/author/create")
    AuthorDTO createAuthor(@RequestBody AuthorCreateDTO authorCreateDTO) {
        return authorService.createAuthor(authorCreateDTO);
    }

    @PutMapping("author/update")
    AuthorDTO updateAuthor(@RequestBody AuthorUpdateDTO authorUpdateDTO) {
        return authorService.updateAuthor(authorUpdateDTO);
    }

    @DeleteMapping("author/delete/{id}")
    void deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
    }
}
