package ru.itgirls.libraryproject.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.itgirls.libraryproject.dto.GenreDTO;
import ru.itgirls.libraryproject.service.GenreService;

@RestController
@RequiredArgsConstructor
public class GenreRestController {

    private final GenreService genreService;

    @GetMapping("/genre/id/{id}")
    GenreDTO getGenreByID(@PathVariable("id") Long id) {
        return genreService.findGenreByID(id);
    }

    @GetMapping("/genre/name/{name}")
    GenreDTO getGenreByName1(@PathVariable("name") String name) {
        return genreService.getGenreByName1(name);
    }

    @GetMapping("/genre/name/sql/{name}")
    GenreDTO getGenreByName2(@PathVariable("name") String name) {
        return genreService.getGenreByName2(name);
    }

    @GetMapping("/genre/name/jpa/{name}")
    GenreDTO getGenreByName3(@PathVariable("name") String name) {
        return genreService.getGenreByName3(name);
    }
}
